const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.getAllUsers = async (req, res) => {
	try {
		const users = await User.find();

		if(!users) {
			return res.status(404).json({
				message: "No users found."
			});
		}

		return res.status(200).json({
			results: users.length, 
			users 
		});

	} catch (error) {
		return res.status(500).json({
			message: "An unexpected error occured."
		});
	}	
}

module.exports.getUser = async (req, res) => {

	try {
		const user = await User.findById(req.params.id);

		if(!user) {
			return res.status(404).json({
				message: "No user found."
			});
		}

		return res.status(200).json({
			user
		});
		
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occured."
		});
	}
}

module.exports.registerUser = async (req, res) => {
	const { email, firstName, lastName, password } = req.body;

	if(!email || !firstName || !lastName || !password) 
	{
		return res.status(422).json({
			message: "Please fill out all the required fields!"
		});
	}

	const hashedPassword = bcrypt.hashSync(password, 10);

	try {
		// Check if the user already exists
		const existingUser = await User.findOne({ email });

		if(existingUser) {
			return res.status(409).json({ 
				message: "User already exist! Please login instead." 
			});
		}

		// Create a New User
		let user = new User({ 
			email, 
			firstName, 
			lastName, 
			password: hashedPassword 
		});

		user = await user.save();
		return res.status(201).json({
			message: "Success!", 
			user 
		});
		
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occured."
		});
	}
};

module.exports.loginUser = async (req, res) => {
	const { email, password } = req.body;

	if (!email || !password) {
		return res.status(400).json({ 
			message: "Please enter your email/password." 
		});
	}

	try {
		const existingUser = await User.findOne({ email });

		if (!existingUser) {
			return res.status(400).json({ 
				message: "Incorrect email or password." 
			});
		}
	
		const isPasswordCorrect = await bcrypt.compare(password, existingUser.password);

		if (!isPasswordCorrect) {
			return res.status(400).json({ 
				message: "Incorrect email or password." 
			});
		}

		const accessToken = auth.createAccessToken(existingUser);
		return res.status(200).json({ 
			message: "Login successful", 
			accessToken 
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({ 
			message: "An unexpected error occured." 
		});
	}
};


module.exports.updateUser = async (req, res, userId) => {
	
	if(req.body.password) {
		req.body.password = bcrypt.hashSync(req.body.password, 10);
	}
	
	try {
		const user = await User.findByIdAndUpdate(req.params.id, 
			{
				$set: req.body
			}, 
			{
				new: true
			}
		);

		if (!user) {
			return res.status(404).json({ 
				message: "User not found!"
			});
		}

		if(userId === req.params.id) {
			return res.status(200).json({ 
				message: "User has been updated successfully."
			});
		}

		return res.status(401).json({
			message: "Authentication Failed!"
		});
		
	} catch (error) {
		console.log(error);
		return res.status(500).json({ 
			message: "An unexpected error occured."
		});
	}
};

module.exports.deleteUser = async (req, res) => {
	try {
		const user = await User.findByIdAndRemove(req.params.id);

		if (!user) {
			return res.status(404).json({ 
				message: "User does not exist!"
			});
		}

		return res.status(200).json({ 
			message: "User has been deleted successfully"
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({ 
			message: "An unexpected error occured."
		});
	}
};

module.exports.changeToAdmin = async (req, res) => {
	
	try {
		const user = await User.findByIdAndUpdate(req.params.id, 
			{
				$set: {
					isAdmin: true
				}
			},

			{
				new: true
			}
		)

		if(!user) {
			return res.status.json({
				message: "User not found."
			});
		}

		return res.status(200).json({
			user
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}




// product controller

const Product = require("../models/Product");

module.exports.createProduct = async (req, res) => {
	const { name, description, price, category, brand } = req.body;

	if(!name || !description || !price || !category || !brand) {
		return res.status(422).json({
			message: "Missing input!"
		});
	}

	let product;
	try {
		const existingProduct = await Product.findOne({ name, price });

		if (existingProduct) {
			return res.status(409).json({ 
				message: "Duplicate product already exist!" 
			});
		}

		product = new Product({
			name,
			description,
			price,
			category,
			brand
		});

		product = await product.save();
		return res.status(201).json({
			message: "Success!",
			product
		}); 

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.getAllProducts = async (req, res) => {
	
	const queryCategory = req.query.category;
	const queryBrand = req.query.brand;

	try {

		let products;

		if (queryCategory && queryBrand) {
			products = await Product.find({ category: queryCategory, brand: queryBrand });
		} else if (queryCategory && !queryBrand) {
			products = await Product.find({ category: queryCategory });
		} else if (queryBrand && !queryCategory) {
			products = await Product.find({ brand: queryBrand });
		} else {
			products = await Product.find();
		}
		
		
		if(products.length === 0) {
			return res.status(404).json({
				message: "No products found!"
			});
		}

		return res.status(200).json({
			results: products.length,
			products
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


module.exports.getActiveProducts = async (req, res) => {
	try {
		const activeProducts = await Product.find({ isActive: true });
	
		if(activeProducts.length === 0) {
			return res.status(404).json({
				message: "No active products found."
			});
		}

		return res.status(200).json({
			results: activeProducts.length,
			activeProducts
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
				message: "An unexpected error occurred."
		});
	}
}

module.exports.getProduct = async(req, res) => {

	try {
		const product = await Product.findById(req.params.id);

		if(!product) {
			return res.status(404).json({
				message: "Product not found!"
			});
		}

		return res.status(200).json({
			product
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
				message: "An unexpected error occurred."
		});
	}
}

module.exports.updateProduct = async (req, res) => {
	const { name, description, price, category, brand } = req.body

	if(!name && !description && !price && !category && !brand) {
		return res.status(422).json({
			message: "Unable to process your request."
		});
	}

	try {
		const updatedProduct = await Product.findByIdAndUpdate(
			req.params.id, 
			{ 
				$set: req.body
			}, 
			{
				new:true
			}
		);

		if(!updatedProduct) {
			return res.status(404).json({
				message: "Product not found!"
			});
		}

		return res.status(200).json({
			message: "The product has been successfully updated.",
			product: updatedProduct
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
				message: "An unexpected error occurred."
		});
	}
}

module.exports.archiveProduct = async (req, res) => {
	const { isActive } = req.body;

	if(isActive) {
		return res.status(422).json({
			message: "Unable to process your request"
		});
	}

	try {
		const archiveProduct = await Product.findByIdAndUpdate(req.params.id, 
			{ 
				isActive 
			}, 
			{
				new:true
			}
		);

		if(!archiveProduct) {
			return res.status(404).json({
				message: "Product not found!"
			});
		}

		return res.status(200).json({
			message: "Product successfully archived!",
			product: archiveProduct
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.deleteProduct = async (req, res) => {
	try {
		const deleteProduct = await Product.findByIdAndRemove(req.params.id);

		if(!deleteProduct) {
			return res.status(404).json({
				message: "Product no longer exist in the database."
			});
		}

		return res.status(200).json({
			message: "Product has been successfully deleted."
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
				message: "An unexpected error occurred."
		});
	}

}


// order controller
const mongoose = require('mongoose');
const Order = require("../models/Order");
const User = require("../models/User");

module.exports.createOrder = async (req, res, userId) => {
	
	try {
		let newOrder = new Order({
			userId: userId,
			products: req.body.products,
			totalAmount: req.body.totalAmount,
			address: req.body.address,
			mobileNo: req.body.mobileNo
		});

		newOrder = await newOrder.save();
		if(!newOrder) {
			return res.status(422).json({
				message: "Unable to create order. Please try again!"
			});
		}

		let existingUser = await User.findById(userId);

		// Storing the Order ID to the user's orderList
		const session = await mongoose.startSession();
		session.startTransaction();
		existingUser.orderList.push(newOrder);
		await existingUser.save({session});
		await newOrder.save({session});
		session.commitTransaction();

		return res.status(201).json({
			message: "Success!",
			order: newOrder
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.cancelOrder = async (req, res) => {
	try {
		const order = await Order.findOneAndRemove({ 
			_id: req.params.orderId,
			status: "pending"
		}).populate("userId");

		if(!order) {
			return res.status(400).json({
				message: "Sorry! You can no longer cancel this order."
			});
		}

		const session = await mongoose.startSession();
		session.startTransaction();
		await order.userId.orderList.pull(order);
		await order.userId.save({ session });
		session.commitTransaction();

		return res.status(200).json({
			message: "Order has been cancelled successfully.",
			order
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.getUserOrders = async (req, res) => {
	try {
		const orders = await Order.find({userId: req.params.userId});

		if(!orders) {
			return res.status(404).json({
				message: "You haven't placed any orders yet."
			});
		}

		return res.status(200).json({
			results: orders.length,
			orders
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


module.exports.getAllOrders = async (req, res) => {
	try {
		const orders = await Order.find();

		if(!orders) {
			return res.status(404).json({
				message: "No orders found."
			});
		}

		return res.status(200).json({
			results: orders.length,
			orders
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.updateOrderStatus = async (req, res) => {
	const { status } = req.body;
	const id = req.params.orderId;
	try {
		const order = await Order.findByIdAndUpdate(id,
			{
				$set: req.body
			},

			{
				new: true
			}
		)

		if(!order) {
			return res.status(404).json({
				message: "Order not found."
			});
		}

		return res.status(200).json({
			message: "Success!",
			order
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


const Cart = require("../models/Cart");
const Product = require("../models/Product");

module.exports.addProductToCart = async (req, res, userId) => {
	let existingUserCart;

	try {

		const isProductActive = await Product.findOne({ _id: req.body.productId, isActive:false});
		
		if(isProductActive) {
			return res.status(422).json({
				message: "The product is currently unavailable."
			});
		}

		existingUserCart = await Cart.findOne({ userId: userId });
		if(existingUserCart) {

			const isProductAdded = await existingUserCart.products.find(c => c.productId == req.body.productId);

			if(isProductAdded) {
				await Cart.findOneAndUpdate({ userId: userId, "products.productId": req.body.productId}, {
					$set: {
						"products.$": {
							...req.body,
							quantity: isProductAdded.quantity + req.body.quantity,
							subtotal: isProductAdded.price * (isProductAdded.quantity + req.body.quantity)
						}		
					}
				});
				
				return res.status(200).json({
					message: "Product has been added successfully to your cart."
				});
			}

			existingUserCart = await Cart.findOneAndUpdate({ userId: userId  }, {
				$push: {
					"products": {
						...req.body,
						subtotal: req.body.price * req.body.quantity
					}
				}
			});

			return res.status(200).json({
				message: "Product has been added successfully to your cart."
			});
		}

		let cart = new Cart({
			userId: userId,
			products: {
				...req.body,
				subtotal: req.body.price * req.body.quantity
			}
		});

		cart = await cart.save();

		if(!cart) {
			return res.status(422).json({
				message: "Unable to process the request."
			});
		}

		return res.status(200).json({
			message: "Product has been added successfully to your cart."
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


module.exports.updateCartQuantity = async (req, res) => {
	const userId = req.params.userId;

	try {
		const updatedCart = await Cart.findOneAndUpdate(
			{ 
				userId: userId, 
				"products.productId": req.body.productId
			},
			{
				$set: {
					"products.$.quantity": req.body.quantity,
					"products.$.subtotal": req.body.quantity * req.body.price
				}
			},
			{new:true}
		);
		return res.status(200).json({
			message: "Updated Successfully.",
			cart: updatedCart
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.removeProductsFromCart = async (req, res) => {
	const userId = req.params.userId;

	try {
		const userCart = await Cart.findOneAndUpdate(
			{ 
				userId: userId, 
				"products.productId": req.body.productId 
			},
			{
				$pull: {
					products: {
						productId: req.body.productId
					}
				}
			}
			);
		const count = userCart.products.length;

		if(count == 1) {
			await Cart.findOneAndRemove({userId: userId});
			return res.status(200).json({
				message: "Your cart is empty."
			});
		}

		return res.status(200).json({
			message: "Product has been deleted from your cart."
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}

module.exports.getCart = async (req, res) => {
	const userId = req.params.userId;
	try {
		const cart = await Cart.findOne({userId});

		if(!cart) {
			return res.status(404).json({
				message: "You cart is empty."
			});
		}

		const count = cart.products.length;

		if(count == 0) {
			await Cart.findByIdAndRemove(userId);
			return res.status(200).json({
				message: "Your cart is empty."
			});
		}

		return res.status(200).json({
			cart
		});
	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


module.exports.getTotalAmount = async (req, res) => {
	try {
		const userCart = await Cart.findOne({userId: req.params.userId});
		if(!userCart) {
			return res.status(404).json({
				message: "No Cart found for this user."
			});
		}
		const getTotalAmount = userCart.products.reduce((total, item) => total + item.subtotal, 0)
		return res.status(200).json({
			total: getTotalAmount
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({
			message: "An unexpected error occurred."
		});
	}
}


const Admin = require("../models/Admin");
const bcrypt = require('bcrypt');
const auth = require("../auth");

module.exports.addAdmin = async (req, res) => {

	const{ email, username, password } = req.body;
	let existingAdmin;
	let admin;
	const hashedPassword = bcrypt.hashSync(password, 10);

	if(!email || !username || !password) 
	{
		return res.status(422).json({
			message: "Please fill out all the fields!"
		});
	}

	try {

		existingAdmin = await Admin.findOne({ email });

		if(existingAdmin) {
			return res.status(400).json({ 
				message: "An admin with the same email already exists!"
			});
		}

		existingAdmin = await Admin.findOne({ username });

		if(existingAdmin) {
			return res.status(400).json({ 
				message: "Username already in use! Please create another one."
			});
		}

		admin = new Admin({ 
			email, 
			username, 
			password:hashedPassword 
		});

		admin = await admin.save();

		return res.status(201).json({
			message: "Success", 
			admin 
		});

	} catch (error) {
		 console.log(error);
		 return res.status(500).json({
		 	message: "An unexpected error occurred."
		 });
	}	
}

module.exports.adminLogin = async (req, res) => {
	const { emailOrUsername, password } = req.body;

	if (!emailOrUsername || !password) {
		return res.status(412).json({ 
			message: "Email/Username and/or password must not be empty" 
		});
	}

	let existingAdmin;
	try {
		// Find admin by email or username
		existingAdmin = await Admin.findOne(
			{
				$or: [
						{ 
							email: emailOrUsername 
						}, 
						{ 
							username: emailOrUsername 
						}
					],
			}
		);

		if (!existingAdmin) {
			return res.status(400).json({ 
				message: "Username/Email or password is incorrect." 
			});
		}

		const isPasswordCorrect = bcrypt.compareSync(password, existingAdmin.password);

		if (!isPasswordCorrect) {
			return res.status(400).json({ 
				message: "Username/Email or password is incorrect." 
			});
		}

		return res.status(200).json({ 
			message: "Success! You are now logged in as an admin.",
			access: auth.createAccessToken(existingAdmin) 
		});

	} catch (error) {
		console.log(error);
		return res.status(500).json({ 
			message: "An unexpected error occurred." 
		});
	}
};


// module.exports.getAllAdmin = async (req, res) => {
// 	let admin;

// 	try {
// 		admin = await Admin.find();

// 		if(!admin) {
// 			return res.status(404).json({
// 				message: "No Admins found!"
// 			});
// 		}

// 		return res.status(200).json({
// 			results: admin.length,
// 			admin
// 		});

// 	} catch (error) {
// 		console.log(error);
// 		return res.status(500).json({ 
// 			message: "An unexpected error occurred." 
// 		});
// 	}
// }
