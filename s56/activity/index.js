console.log("Hello World");


 let letter;
 let sentence;
function countLetter(letter, sentence) {
    
    let result = 0;
   

   // Check first whether the letter is a single character.
// If letter is invalid, return undefined.
    if (letter.length !== 1) {
        return undefined;
    }

      // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    for (let i = 0; i < sentence.length; i++) {
       
        if (sentence[i] === letter) {
            result++;
        }
    }

    return result;
}

console.log(countLetter("b", "babylove"));


function isIsogram(text) {
    // The function should disregard text casing before doing anything else.
    text = text.toLowerCase();
    // An isogram is a word where there are no repeating letters.
    

    for (let i = 0; i < text.length; i++) {
            // If the function finds a repeating letter, return false. Otherwise, return true.
            if (text.indexOf(text[i]) !== i) {
                // If it does, the text is not an isogram.
                return false;
            }
        }
        return true;
    }

    console.log (isIsogram("frog"));

function purchase(age, price) {
    
    // Return undefined for people aged below 13.
    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21 || age >= 65) {
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        let discountedPrice = price * 0.8;
        return Math.round(discountedPrice).toString();
    } else {
         // Return the rounded off price for people aged 22 to 64.
        // The returned value should be a string.
        return Math.round(price).toString();
    }
}

console.log(purchase(25,36));
    



    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    function findHotCategories(items) {
    let hotCategories = {};
    for (let i = 0; i < items.length; i++) {
        if (items[i].stocks === 0) {
            hotCategories[items[i].category] = true;
        }
    }
    return (hotCategories);
}




    

function findFlyingVoters(candidateA, candidateB) {
    

    const setA = new Set(candidateA);
    const commonVoters = [];

    for (let voter of candidateB) {
        if (setA.has(voter)) {
          commonVoters.push(voter);
        }
    }

  return commonVoters;

    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}











/*module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};*/