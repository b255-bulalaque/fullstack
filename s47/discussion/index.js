const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const fullName = txtFirstName.concat(txtLastName);
const spanFullName = document.querySelector('#span-full-name');

fullName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = fullName.value;
})

txtFirstName.addEventListener('keyup', (event) =>{
	console.log(event.target);
	console.log(event.target.value);
})

txtLastName.addEventListener('keyup', (event) =>{
	console.log(event.target);
	console.log(event.target.value);
})