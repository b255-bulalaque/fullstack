import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
export default function Login(props) {


// Allows us to consue the User context object and it's properties to use for user validation

    const {user, setUser} = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);
    


    function authenticate(e) {

    // Prevents page redirection via form submission
    e.preventDefault();

    // Process a fetch request to the corresponding backend API
    // The header information "Content-Type" is used to specify that the information bing sent to the backend will be sent in the form of JSON
    // The fetch request will communicate with our backend application providing it with a stringified JSON
    fetch('http://localhost:4000/users/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    })

    .then(res => res.json())
    .then(data => {
        console.log(data);

        // iF NO user information is found, the "access" property will not be available and will return undefined
        if (typeof data.access !== "undefined"){
            localStorage.setItem('token', data.access);
            retrieveUserDetails(data.access);

            Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to Zuitt!"
            });
        } else {
            Swal.fire({
                title: "Authentication Failed",
                icon: "error",
                text: "Check your login details and try again."
            })
        }
    })

    // Set the email of the authenticated user in the Local storage
    // Syntax
    // localStorage.setItem('email', email);
    
    // Set the global user state to have properties obtained from local stoarage
    // Though access to the user information can be done via the localstorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page
    // When  state change components are rerender and the appnavbar component will be updated based on the user credentials 

   /* setUser({
        email:localStorage.getItem('email')

    })*/

    // Clear input fields after submission
    setEmail('');
    setPassword('');

    console.log(`${email} has been verified! Welcome back!`);

     }

    const retrieveUserDetails = (token) => {
        
        // The token will be sent as part of the requests header information
        fetch ('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })


        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin

            })
        })
    }

    useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

    }, [email, password]);

    return (

       
            (user.id !== null) ?
            <Navigate to="/courses"/>
            :
                <Form onSubmit={(e) => authenticate(e)}>
               <Form.Group controlId="userEmail">
                   <Form.Label>Email address</Form.Label>
                   <Form.Control 
                       type="email" 
                       placeholder="Enter email"
                       value={email}
                    onChange={(e) => setEmail(e.target.value)}
                       required
                   />
               </Form.Group>

               <Form.Group controlId="password">
                   <Form.Label>Password</Form.Label>
                   <Form.Control 
                       type="password" 
                       placeholder="Password"
                       value={password}
                    onChange={(e) => setPassword(e.target.value)}
                       required
                   />
               </Form.Group>

               { isActive ?

                   <Button variant="primary" type="submit" id="submitBtn">
                       Submit
                   </Button>
                   :
                   <Button variant="primary" type="submit" id="submitBtn" disabled>
                       Submit
                   </Button>
            }
           </Form>


        )
    }

