import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){
	// Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider
	const {unsetUser, setUser} = useContext(UserContext);

	// Clear the localstorage of the user's information
	unsetUser();

	// placing the "setUser" setter function inside of a useEffect is necessary because of updates within REACTJS that a state of another component cannot be updated while trying to render a different component
	// By adding the useEffet, this will allow logout page to render first before trigerring the useEffect which changes that state of the user

	useEffect(() =>{
		// Set the user state back to it's original value
		setUser({id:null});
	})


	return(
		<Navigate to='/login' />
	)
}
