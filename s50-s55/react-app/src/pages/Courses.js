import { Fragment,useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';



export default function Courses(){
	
	// checks to see if the mock data was captured
	// console.log(coursesData);
	// console.log(coursesData[0]);


	// The "coourse" in the coursecard components is call a prop which is a shorthand for "property" since components are considered as objects in reactJS
	// The curly braces ({})are used for props to signify that we are providing information using JvaScript expressions rather than hard coded values
	// We can pass information from one component to another using props.This is referred to as props drilling 
	
	const [ courses, setCourses ] = useState([])

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setCourses(data.map(course => {
				return(
					
					<CourseCard key={course.id} courseProp= {course} />
					
				)
			}))
		});
	}, []);




	return (
		<Fragment>
			{courses}
		</Fragment>
	)
	
}