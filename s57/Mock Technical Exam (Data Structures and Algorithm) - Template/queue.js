let collection = [];


function print() {
  return (collection);
}



function enqueue(name) {
  collection.push(name);
  return collection;
}



function dequeue(name) {
  collection.shift(name);
  return collection;
 
}



function front(){
  return collection[0];
}



function size() {
  return collection.length;
}


function isEmpty() {

  if(collection.length > 0){
    return false;

  } else {
    return true;
  }
}



module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};