
import React from 'react';
import { Link } from 'react-router-dom';


export default function NotFound(){


	return(
			<div>
				<h1>Page Not Found</h1>
			    <p>Go back to <Link to="/">homepage</Link>.</p> 
			</div>    
		)
}

 

        