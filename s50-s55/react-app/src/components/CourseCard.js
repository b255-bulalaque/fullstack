import { Button, Card } from 'react-bootstrap';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}){

    // Check to see if the data was successfully passed
    // console.log(props);
    // Every component receives informaton in a form of an object
    // console.log(typeof props);


    // Use the state hook from this component to be able to store its state
    // State are used to keep track of information realted to individual components
    const [count, setCount]= useState(0);
    // Using the state hook returns an array with th first element being aa value and the second element as function thats used to change the valuwe of the first element
    console.log(useState(0));

    // Function that keep tracks of the enrollees for a course
    // By default Javascript is synchronous it executes code from the top of the file all the way down 
    // The setter function for useState are asynchronous allowing it to execute seperately from other codes in the program
    // The "setCount" function is being executed while the "console.log" is already completed 
    const [seat, setSeat]= useState(30);
    console.log(useState(30));

/*function enroll(){
    setCount(count + 1);
    console.log('Enrollees: ' + count);
    
    setSeat (seat - 1);
    console.log('Seat: ' + seat);
    if ( seat === 0 )
        alert ("No more Seats");
    


}
*/
    const {name, description, price, _id} = courseProp;
    return (
         <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Link className="btn btn-primary" to={`/coursesd/${_id}`}>Details</Link>

               
            </Card.Body>
        </Card>

    )
}

// proptypes= validation tool/evaluation of data (right data type)
CourseCard.propTypes = {
    // The shape method is used to check if a prop object conforms to a specific shape
    course: PropTypes.shape({
        // define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })
}
