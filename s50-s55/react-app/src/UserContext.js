import React from 'react';

// Creates a context object
// A context object as the name states is a dta type of an object that can be used to store information that can be shared to other components within this app
// The onext object is a diferent approach to passing infromation between components and allows easier access by avoiding the use of prop-drilling

const UserContext = React.createContext();

// The provider component allow the other component to comsume/use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;